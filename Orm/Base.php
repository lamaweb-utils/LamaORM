<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 5.8.2015
 * Time: 17:24
 */

namespace Lama\ORM;


use \Lama\Exceptions\BadProperty;

abstract class Base extends Iterator {
	const CONFIG_PRIMARY_ALIAS = 'CONFIG_PRIMARY_ALIAS';
	const CONFIG_PRIMARY_DB_FIELD = 'CONFIG_PRIMARY_DB_FIELD';
	const CONFIG_DB_TABLE = 'CONFIG_TABLE';
	const CONFIG_CLASS = 'CONFIG_CLASS';

	protected $config = array();
	protected $data = array();
	protected $aliases = array();
	protected $dbFields = array();
	/** @var \Lama\Storage\Php */
	public static $storage = null;
	/** @var \Lama\QueryBuilder\PdoQueryBuilder */
	protected static $queryBuilder = null;
	protected $changed = array();
	protected $isLoaded = false;
	protected $children = array();
	protected $childrenData = array();

	const CHILD_NAME = 'name';
	const CHILD_CLASS = 'class';
	const CHILD_LOCAL_KEY = 'name';
	const CHILD_FOREIGN_KEY = 'foreign';

	abstract function setUp();

	public function __construct($primaryId = null){
		$this->config[self::CONFIG_CLASS] = $className = get_class($this);
		if(!self::$storage->isStored($className)){
			$this->setUp();
			self::$storage->store($className, $this);
		} else {
			self::$storage->restore($className, $this);
		}

		if(!empty($primaryId)){
			$this->{$this->config[self::CONFIG_PRIMARY_ALIAS]} = $primaryId;
			$this->load();
		}
	}
	public function __call($name, $arguments)
    {
            if(preg_match('#getFirstChild(.*)#', $name, $matches)){
            $childName = $matches[1];
            $children = $this->childrenData[lcfirst($childName)];
            return reset($children);
        }
    }

	public function getConfig(){
		return $this->config;
	}
	public function setConfig($config){
		return $this->config = $config;
	}

	public function getAllAliases($imploder = null){
		if(is_null($imploder)){
			return $this->aliases;
		}
		return implode($imploder, $this->aliases);
	}
	public function setAliases($aliases){
		return $this->aliases = $aliases;
	}
	public function getAlias($dbField){
		return $this->aliases[$dbField];
	}

	/**
	 * @param null $imploder
	 * @return array|string
	 */
	public function getAllDbFields($imploder = null, $withTable = false){
		$table = $this->getDbTable();
		if(is_null($imploder)){
			if($withTable){
				$return = array();
				foreach($this->dbFields AS $alias => $dbField){
					$return[$alias] = $table.'.'.$dbField;
				}
				return $return;
			} else {
				return $this->dbFields;
			}
		}
		if($withTable){
			return $table . '.' . implode($imploder . ' ' . $table . '.', $this->dbFields);
		} else{
			return implode($imploder, $this->dbFields);
		}
	}
	public function getDbFields(array $fields, $imploder = null, $withTable = false){
		$selected = array();
		foreach($fields AS $field){
			$selected[] = $this->getDbField($field, $withTable);
		}
		if(is_null($imploder)){
			return $selected;
		}
		return implode($imploder, $selected);
	}
	public function getDbField($alias, $withTable = false){
		if(!$withTable){
			return $this->dbFields[$alias];
		} else {
			return $this->getDbTable().'.'.$this->dbFields[$alias];
		}
	}
	public function setDbFields($dbFields){
		return $this->dbFields = $dbFields;
	}

	public static function setStorage(\Lama\Storage\IStorage $storage){
		self::$storage = $storage;
	}

	public static function setQueryBuilder(\Lama\QueryBuilder\IQueryBuilder $queryBuilder){
		self::$queryBuilder = $queryBuilder;
	}

	public static function getQueryBuilder(){
		return self::$queryBuilder;
	}

	public function isLoaded(){
		return $this->isLoaded;
	}
	public function setIsLoaded($isLoaded = true){
		$this->isLoaded = $isLoaded;
	}

    public function load(Array $result = null){
        if(!$result){
            $result = self::$queryBuilder->load($this);
        }
        if(!empty($result)){
            foreach($result AS $dbField => $value){
                $this->{$this->aliases[$dbField]} = $value;
            }
            $this->setIsLoaded(true);
            $this->clearChanged();
            return true;
        }
        return false;
    }
    
	public function loadMultiple(){
		return self::$queryBuilder->loadMultiple($this);
	}

	public function save($forceSave = false){
		if($forceSave || empty($this->data[$this->config[self::CONFIG_PRIMARY_ALIAS]])){
			return self::$queryBuilder->insert($this);
		} else {
			return self::$queryBuilder->update($this);
		}
	}

	public function delete(){
		return self::$queryBuilder->delete($this);
	}
	public function deleteByPrimaryKey(){
		return self::$queryBuilder->deleteByPrimaryKey($this);
	}


	public function addColumn($alias, $dbField){
		$this->aliases[$dbField] = $alias;
		$this->dbFields[$alias] = $dbField;
	}

	public function addPrimaryColumn($alias, $dbField){
		$this->config[self::CONFIG_PRIMARY_ALIAS] = $alias;
		$this->config[self::CONFIG_PRIMARY_DB_FIELD] = $dbField;
		$this->addColumn($alias, $dbField);
	}

	public function setDbTable($dbTable){
		$this->config[self::CONFIG_DB_TABLE] = $dbTable;
	}

	public function __set($name, $value){
		if(isset($this->dbFields[$name])){
			$this->data[$name] = $value;
			$this->setIsLoaded(false);
		} elseif(isset($this->children[$name])){
			$this->childrenData[$name] = $value;
		} else {
			throw new BadProperty('Bad property "'.$name.'" used');
		}
	}

	public function __get($name){
		if(isset($this->dbFields[$name])){
			return $this->data[$name];
		}
		if(isset($this->children[$name])){
			if(!isset($this->childrenData[$name])){
				return array();
			}
			return $this->childrenData[$name];
		}
		throw new BadProperty('Bad property "'.$name.'" requested');
	}

	public function getData(){
		return $this->data;
	}

	public function getDbTable(){
		return $this->config[self::CONFIG_DB_TABLE];
	}

	public function getPrimaryAlias(){
		return $this->config[self::CONFIG_PRIMARY_ALIAS];
	}
	public function clearChanged(){
		$this->changed = array();
	}

	public function addChild($childName, $childClass, $localKey, $foreignKey){
		if(isset($this->aliases[$childName])){
			throw new BadProperty('Property with name "' . $childName . '" already exists');
		}
		$this->children[$childName] = array(
			self::CHILD_NAME => $childName,
			self::CHILD_CLASS => $childClass,
			self::CHILD_LOCAL_KEY => $localKey,
			self::CHILD_FOREIGN_KEY => $foreignKey
		);
		$this->childrenData[$childName] = array();
	}
	public function getChild($childName){
		return $this->children[$childName];
	}

	public function getChildren()
	{
		return $this->children;
	}
	public function setChildren(Array $children)
	{
		$this->children = $children;
	}

	/**
	 * Magic Method __isset
	 * @param $property
	 * @return bool
	 */
	public function __isset($property)
	{
		return isset($this->data[$property]) || isset($this->childrenData[$property]);
	}

	/**
	 * Magic Method __empty
	 * @param $property
	 * @return bool
	 */
	public function __empty($property)
	{
		return empty($this->data[$property]) || empty($this->childrenData[$property]);
	}

	/**
	 * @inheritdoc
	 */
	protected function getIterableName()
	{
		return 'data';
	}

	public function __sleep()
	{
		return array('data', 'childrenData');
	}
	public function __wakeup()
	{
		$this->__construct();
	}
}