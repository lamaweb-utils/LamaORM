<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 5.8.2015
 * Time: 22:11
 */

namespace Lama\Connector;


interface IConnector {
	public function query($query, $params = array(), $fetchMode = \PDO::FETCH_ASSOC);
	public function exec($query, $params);
	public function getLastInsertedId();
	public function getAllQueries();
}