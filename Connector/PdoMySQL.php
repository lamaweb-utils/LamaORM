<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 5.8.2015
 * Time: 22:14
 */

namespace Lama\Connector;


class PdoMySQL implements \Lama\Connector\IConnector {
	protected $connection = null;
	private static $queries = array();

	public function __construct($host, $db, $user, $password){
		$dsn = 'mysql:dbname=' . $db . ';host=' . $host.';charset=utf8';
		try {
			$this->connection = new \PDO($dsn, $user, $password);
		} catch (\PDOException $e) {
			throw new \Exception('Connection failed: ' . $e->getMessage());
		}
	}

	public function query($query, $params = array(), $fetchMode = \PDO::FETCH_ASSOC){
		$db = $this->connection->prepare($query);
		$start = microtime(true);
		$db->execute($params);
		$end = microtime(true);
		if($db->errorCode() > 0){
			throw new \PDOException($db->errorInfo()[2]);
		}
		self::$queries[] = array('query' => $query, 'params' => $params, 'time' => ($end - $start));
		return $db->fetchAll($fetchMode);
	}

	public function exec($query, $params = array())
	{
		$db = $this->connection->prepare($query);
		$start = microtime(true);
		$db->execute($params);
		$end = microtime(true);
		if($db->errorCode() > 0){
			throw new \PDOException($db->errorInfo()[2]);
		}
		self::$queries[] = array('query' => $query, 'params' => $params, 'time' => ($end - $start));
		return $db->rowCount();
	}
	public function getLastInsertedId()
	{
		return $this->connection->lastInsertId();
	}
	public function getAllQueries()
	{
		return self::$queries;
	}
}