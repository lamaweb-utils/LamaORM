<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 5.8.2015
 * Time: 22:11
 */

namespace Lama\QueryBuilder;


interface IQueryBuilder {
	public function load(\Lama\ORM\Base $orm);
	public function loadMultiple(\Lama\ORM\Base $orm);
	public function insert(\Lama\ORM\Base $orm);
	public function update(\Lama\ORM\Base $orm);
	public function delete(\Lama\ORM\Base $orm);
	public function getAllQueries();
}