<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 17.5.14
 * Time: 23:39
 */

namespace Lama\QueryBuilder;

use \Lama\ORM;

class Query {
	public static $connector = '\dibi';

	public static function loadByORM(ORM $orm)
	{
		$query = self::prepareQuery($orm);
		/** @var \dibi $connector */
		$connector = self::$connector;
		$result = $connector::query($query);
		self::fillOrm($orm, $result);
		$orm->resetChanged();
	}

	private static function prepareQuery(ORM $orm)
	{
		$query = self::prepareSelect($orm);
		$query .= self::prepareFrom($orm);
		$query .= self::prepareWhere($orm);
		return $query;
	}

	private static function prepareSelect(ORM $orm)
	{
		$storage = $orm->getStorage();
		$dbFields = $storage::getAllDbFields($orm);
		return 'SELECT ' . implode(',', $dbFields);
	}

	private static function prepareFrom(ORM $orm)
	{
		$storage = $orm->getStorage();
		return ' FROM ' . $storage::getTable($orm);
	}

	private static function prepareWhere(ORM $orm)
	{
		$storage = $orm->getStorage();
		$dbFields = $storage::getAllDbFields($orm);
		$data = $orm->getData();

		$where = array();
		foreach($data AS $key => $value){
			if(!is_null($value)){
				$where[] = $dbFields[$key] . ' = \'' . $value . '\'';
			}
		}
		return ' WHERE ' . implode(' AND ', $where);
	}

	/**
	 * @param ORM $orm
	 * @param $result
	 */
	private static function fillOrm(ORM $orm, $result)
	{
		/** @var \DibiResult $result */
		$results = $result->fetch();
		$storage = $orm->getStorage();
		$dbFields = $storage::getAllAliases($orm);
		foreach($dbFields AS $dbField => $alias){
			$orm->$alias = $results->$dbField;
		}
		$orm->setLoaded(true);
	}

	public static function saveByORM(ORM $orm)
	{
		/** @var \dibi $connector */
		$connector = self::$connector;
		$primaryKey = $orm->getPrimaryKey();
		if(is_null($orm->$primaryKey)){
			$query = self::insert($orm);
			$result = $connector::query($query);
			$orm->$primaryKey = $connector::insertId();
		} else {
			$query = self::update($orm);
			dump($query);
			$result = $connector::query($query);
		}
		$orm->resetChanged();
		return (bool)$connector::getAffectedRows();
	}

	private static function insert(ORM $orm)
	{
		$storage = $orm->getStorage();
		$values = array();
		$dbFields = $storage::getAllDbFields($orm);
		$query = 'INSERT INTO ' . $storage::getTable($orm);
		foreach($orm->getData() AS $alias => $value){
			if(!is_null($value)){
				$values[$dbFields[$alias]] = $value;
			}
		}
		$query .= '('.implode(',', array_keys($values)).')';
		$query .= ' VALUES (\''.implode('\',\'', $values).'\');';
		return $query;
	}

	private static function update(ORM $orm)
	{
		$storage = $orm->getStorage();
		$dbFields = $storage::getAllDbFields($orm);
		$changedData = $orm->getChanged();
		$primaryKeyAlias = $orm->getPrimaryKey();
		$changes = array();

		$query = 'UPDATE ' . $storage::getTable($orm);
		foreach($changedData AS $alias){
			$value = $orm->$alias;
			if(is_null($value)){
				$changes[] = $dbFields[$alias] . ' = NULL';
			} else {
				$changes[] = $dbFields[$alias] . ' = \'' . $value . '\'';
			}
		}
		$query .= ' SET ' . implode($changes);
		$query .= ' WHERE ' . $dbFields[$primaryKeyAlias]. ' = ' . $orm->$primaryKeyAlias . ' LIMIT 1;';
		return $query;
	}
}