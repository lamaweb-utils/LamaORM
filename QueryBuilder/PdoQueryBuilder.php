<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 5.8.2015
 * Time: 22:11
 */

namespace Lama\QueryBuilder;

class PdoQueryBuilder implements \Lama\QueryBuilder\IQueryBuilder{
	/** @var \Lama\Connector\PdoMySQL */
	protected $connector = null;

	public function __construct(\Lama\Connector\IConnector $connector){
		$this->connector = $connector;
	}

	public function getConnector()
	{
		return $this->connector;
	}

	public function load(\Lama\ORM\Base $orm){
		$params = array();
		$sql = $this->loadSql($orm, $params) . ' LIMIT 1';
		$result = $this->connector->query($sql, $params);
		if(!isset($result[0])){
			return array();
		}
		return $result[0];
	}
	public function loadMultiple(\Lama\ORM\Base $orm){
		$params = array();
		$sql = $this->loadSql($orm, $params);
		$results = $this->connector->query($sql, $params);

		$return = array();
		foreach($results AS $result){
			/** @var $newORM \Lama\ORM\Base */
			$newORM = new $orm;
			foreach($result AS $dbField => $value){
				$newORM->{$newORM->getAlias($dbField)} = $value;
				$newORM->clearChanged();
				$newORM->setIsLoaded();
			}
			$return[] = $newORM;
		}
		return $return;
	}
	protected function loadSql(\Lama\ORM\Base $orm, &$params){
		$sql = 'SELECT ' . implode(',', $orm->getAllDbFields()) . ' FROM ' . $orm->getDbTable();
		$data = $orm->getData();
		foreach($data As $alias => $value){
			if(!is_null($value)){
				$paramName = ':'.$alias;
				$params[$paramName] = $value;
				$where[] = $orm->getDbField($alias) . '=' . $paramName;
			}
		}
		if(!empty($where)){
			$sql .= ' WHERE ' . implode(' AND ', $where);
		}
		return $sql;
	}
	public function insert(\Lama\ORM\Base $orm){
		$columns = $params = array();
		$sql = 'INSERT INTO ' . $orm->getDbTable() . ' SET ';
		$data = $orm->getData();
		foreach($data AS $alias => $value){
			if($sql != $orm->getConfig()[$orm::CONFIG_PRIMARY_ALIAS]) {
				$paramName = ':' . $alias;
				$params[$paramName] = $value;
				$columns[] = $orm->getDbField($alias) . '=' . $paramName;
			}
		}
		$sql .= implode(',', $columns);
		$affectedRows = $this->connector->exec($sql, $params);
		$lastId = $this->connector->getLastInsertedId();
		if($lastId && (bool)$affectedRows){
			$orm->{$orm->getPrimaryAlias()} = $lastId;
			$orm->clearChanged();
			$orm->setIsLoaded(true);
		}
		return (bool)$affectedRows;
	}
	public function update(\Lama\ORM\Base $orm){
		$columns = $params = array();
		$sql = 'UPDATE ' . $orm->getDbTable() . ' SET ';
		$data = $orm->getData();
		foreach($data AS $alias => $value){
			if($sql != $orm->getConfig()[$orm::CONFIG_PRIMARY_ALIAS]){
				$paramName = ':' . $alias;
				$params[$paramName] = $value;
				$columns[] = $orm->getDbField($alias).'=' . $paramName;
			}
		}
		$sql .= implode(',', $columns);
		$sql .= ' WHERE ' . $orm->getConfig()[$orm::CONFIG_PRIMARY_DB_FIELD] . '=' . $orm->{$orm->getConfig()[$orm::CONFIG_PRIMARY_ALIAS]};
		$affectedRows = $this->connector->exec($sql, $params);
		if((bool)$affectedRows){
			$orm->clearChanged();
			$orm->setIsLoaded(true);
		}
		return (bool)$affectedRows;
	}
	public function deleteByPrimaryKey(\Lama\ORM\Base $orm){
		$params = array();
		$sql = 'DELETE FROM ' . $orm->getDbTable() . ' WHERE ';
		$params[':primaryKey'] = $orm->{$orm->getConfig()[$orm::CONFIG_PRIMARY_ALIAS]};
		$sql .= $orm->getConfig()[$orm::CONFIG_PRIMARY_DB_FIELD] . '= :primaryKey';
		$affectedRows = $this->connector->exec($sql, $params);
		return (bool)$affectedRows;
	}
	public function delete(\Lama\ORM\Base $orm){
		$params = array();
		$where[] = $orm->getConfig()[$orm::CONFIG_PRIMARY_DB_FIELD] . '=' . $orm->{$orm->getConfig()[$orm::CONFIG_PRIMARY_ALIAS]};
		$sql = 'DELETE FROM ' . $orm->getDbTable() . ' WHERE ';
		$data = $orm->getData();
		foreach($data AS $alias => $value){
			$paramName = ':' . $alias;
			$params[$paramName] = $value;
			$where[] = $orm->getDbField($alias).'=' . $paramName;
		}
		$sql .= implode(' AND ', $where);
		$affectedRows = $this->connector->exec($sql, $params);
		return (bool)$affectedRows;
	}
	public function getAllQueries()
	{
		return $this->connector->getAllQueries();
	}
}