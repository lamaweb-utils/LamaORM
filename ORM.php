<?php

namespace Lama;
/**
 * Class LamaORM
 *
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 27.2.14
 * Time: 19:50
 *
 */
abstract class ORM extends Iterator{

	private $storage = '\Lama\Storage\Safe';
	private $primaryKey;
	private $className = null;
	private $data = array();
	private $loaded = false;
	private $changed = array();

	/**
	 * Nastaveni dcerinne tridy (propojeni modelu s DB definici)
	 */
	abstract protected function setUp();

	public function __construct($primaryValue = null)
	{
		$storage = $this->storage;
		/** @var $storage Storage\Safe */
		if(!$storage::classExists($this->className)){
			$this->className = get_class($this);
			$storage::addClassName($this->className);
			$this->setUp();
		}
		if(!is_null($primaryValue)){
			$this->data[$this->primaryKey] = $primaryValue;
			$this->load();
		}
	}

	/**
	 * nastaví hodnotu orm
	 * @param mixed $column
	 * @param mixed $value
	 * @return bool
	 */
	public function __set($column, $value)
	{
		$this->checkColumnExist($column);
		$this->data[$column] = $value;
		$this->changed[] = $column;
		return true;
	}

	/**
	 * vraci hodnotu orm
	 * @param $column
	 * @return mixed
	 */
	public function __get($column)
	{
		$this->checkColumnExist($column);
		return $this->data[$column];
	}

	/**
	 * ověřování zda existuje sloupec
	 * @param $column
	 * @throws Exceptions\ColumnNotFoundException
	 */
	private function checkColumnExist($column)
	{
		$storage = $this->storage;
		/** @var $storage Storage\Safe */
		if(!$storage::aliasExists($this, $column)){
			throw new Exceptions\ColumnNotFoundException('column '.$column.' nenalezen');
		}
	}

	public function addColumn($dbField, $alias){
		$storage = $this->storage;
		/** @var $storage Storage\Safe */
		$storage::addColumn($this, $dbField, $alias);
		$this->data[$alias] = null;
	}

	public function addPrimaryColumn($dbField, $alias)
	{
		$this->addColumn($dbField, $alias);
		$this->primaryKey = $alias;
	}

	public function setDbTable($table)
	{
		$storage = $this->storage;
		/** @var $storage Storage\Safe */
		$storage::setTable($this, $table);
	}

	public function getDbTable()
	{
		$storage = $this->storage;
		/** @var $storage Storage\Safe */
		return $storage::getTable($this);
	}

	/**
	 * kvůli echu
	 * @return string
	 */
	public function __toString() {
		$return = '';
		foreach($this->data AS $alias => $value){
			$return .= $alias.':'. $value." <br />\n";
		}
		return $return;
	}

	public function load()
	{
		$queryBuilder = new QueryBuilder\Query;
		$queryBuilder::loadByORM($this);
	}

	public function save()
	{
		$queryBuilder = new QueryBuilder\Query;
		return $queryBuilder::saveByORM($this);
	}

	/**
	 * @return Storage\Safe
	 */
	public function getStorage()
	{
		return $this->storage;
	}

	public function getData()
	{
		return $this->data;
	}

	public function setLoaded($loaded = false)
	{
		$this->loaded = $loaded;
	}
	public function isLoaded()
	{
		return $this->loaded;
	}

	public function getPrimaryKey()
	{
		return $this->primaryKey;
	}

	/**
	 * @inheritdoc
	 */
	protected function getIterableName()
	{
		return 'data';
	}

	public function resetChanged()
	{
		$this->changed = array();
	}

	public function getChanged()
	{
		return array_unique($this->changed);
	}
}