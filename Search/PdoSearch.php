<?php
/**
 * Created by PhpStorm.
 * User: Petrs
 * Date: 18.8.2015
 * Time: 12:08
 */

namespace Lama\Search;


use Lama\Connector\IConnector;

class PdoSearch extends ASearch{
	const ORDER_ASC = 'ASC';
	const ORDER_DESC = 'DESC';
	public function addChild($childName, $joinType = 'INNER', Array $additionalJoinOptions = array())
	{
		$child = $this->primaryORM->getChild($childName);
		/** @var \Lama\ORM\Base $childORM */
		$childORM = new $child[\Lama\ORM\Base::CHILD_CLASS]();
		$joinOptions = array_merge(array($this->primaryORM->getDbTable().'.'.$child[\Lama\ORM\Base::CHILD_LOCAL_KEY].'=child_'.$childName.'.'.$child[\Lama\ORM\Base::CHILD_FOREIGN_KEY]), $additionalJoinOptions);
//		$joinOptions = array_merge(array($this->primaryORM->getDbTable().'.'.$child[\Lama\ORM\Base::CHILD_LOCAL_KEY].'='.$childORM->getDbTable().'.'.$child[\Lama\ORM\Base::CHILD_FOREIGN_KEY]), $additionalJoinOptions);

		$childColumns = $childORM->getAllDbFields(null);
		$childColumnsPrepared = array();
		foreach( $childColumns AS $childColumnIndex => $childColumn){
			$childColumnsPrepared[$childColumnIndex] = (self::CHILDREN_DB_TABLE_PREFIX) . $childName.'.'.$childColumn;
		}
//		$this->childColumns[$childName] = $childORM->getAllDbFields(null, true);
		$this->childColumns[$childName] = $childColumnsPrepared;
        $primaryORMIndex = array_search($childORM->getPrimaryAlias(), array_values($childORM->getAllAliases()));
        $this->children[$childName][self::CHILDREN_ORM] = $childORM;
		$this->children[$childName][self::CHILDREN_NAME] = $childName;
		$this->children[$childName][self::CHILDREN_JOIN_TYPE] = $joinType;
		$this->children[$childName][self::CHILDREN_PARAMETERS] = $joinOptions;
        $this->children[$childName][self::CHILDREN_ORM_INDEX] = $primaryORMIndex;
	}

	public function in($parameter, Array $values)
	{
		$this->conditions[] = $this->getColumn($parameter) . ' IN (' . implode(',', $this->getAndSetParams($values) ) . ')';
	}
	public function notIn($parameter, Array $values)
	{
		$this->conditions[] = $this->getColumn($parameter) . ' NOT IN (' . implode(',', $this->getAndSetParams($values));
	}
	public function exact($parameter, $value)
	{
		$this->conditions[] = $this->getColumn($parameter) . ' = ' . $this->getAndSetParam($value);
	}
	public function notExact($parameter, $value){
		$this->different($parameter, $value);
	}
	public function different($parameter, $value)
	{
		$this->conditions[] = $this->getColumn($parameter) . ' != ' . $this->getAndSetParam($value);
	}
	public function bigger($parameter, $value)
	{
		$this->conditions[] = $this->getColumn($parameter) . ' > ' . $this->getAndSetParam($value);
	}
	public function biggerEqual($parameter, $value)
	{
		$this->conditions[] = $this->getColumn($parameter) . ' >=' . $this->getAndSetParam($value);
	}
	public function smaller($parameter, $value)
	{
		$this->conditions[] = $this->getColumn($parameter) . ' < ' . $this->getAndSetParam($value);
	}
	public function smallerEqual($parameter, $value)
	{
		$this->conditions[] = $this->getColumn($parameter) . ' <= ' . $this->getAndSetParam($value);
	}

    /**
     * @return \Lama\ORM\Base[]
     */
	public function load(){
		$results = $this->loadAsArray();
		$return = array();
		foreach($results AS $result){
			/** @var \Lama\ORM\Base $orm */
			$orm = new $this->primaryORM();
			foreach($result AS $dbField => $value){
				$orm->{$orm->getAlias($dbField)} = $value;
			}
			$orm->clearChanged();
			$return[] = $orm;
		}
		return $return;
	}

    /**
     * @return array
     */
    public function loadAsArray()
    {
        $query = 'SELECT ' . implode(',', $this->columns) . ' FROM ' . $this->primaryORM->getDbTable();
        if(!empty($this->conditions)){
            $query .= ' WHERE ' . implode(' AND ', $this->conditions);
        }
        if(!empty($this->ordering)){
            $query .= ' ORDER BY ' . implode(',', $this->ordering);
        }
        if(!empty($this->limit)){
            $query .= ' LIMIT ' . $this->limit;
            if(!empty($this->offset)){
                $query .= ' OFFSET' . $this->offset;
            }
        }
        $db = self::getConnector();
        return $db->query($query, $this->params);
    }

	public function loadCountWithChildrenLoaded(){
		$selectedColumnsCount = array();

		$query = 'SELECT COUNT(' . $this->primaryORM->getDbField($this->primaryORM->getPrimaryAlias(), true) . ')';
		$query .= ' FROM ' . $this->primaryORM->getDbTable();

		foreach($this->children AS $child){
			$childName = $child[self::CHILDREN_NAME];
			/** @var \Lama\ORM\Base $childORM */
			$childORM = $child[self::CHILDREN_ORM];
			$query .= ' '.$child[self::CHILDREN_JOIN_TYPE] . ' JOIN ' . $childORM->getDbTable() . ' AS ' . (self::CHILDREN_DB_TABLE_PREFIX) . $childName . ' ON (' . implode(' AND ', $child[self::CHILDREN_PARAMETERS]) . ')';
			$selectedColumnsCount[$childName]['size'] = sizeof($this->childColumns[$childName]);
			$selectedColumnsCount[$childName]['orm'] = $child[self::CHILDREN_ORM];
		}
		/** @var IConnector $db */
		$db = self::getConnector();
		$result = $db->query($query, $this->params, \PDO::FETCH_NUM);
		if(isset($result[0][0])){
			return $result[0][0];
		}
		return null;
	}
	public function loadWithChildrenLoaded($withIndex = false){
		$columns = $this->columns;
		$selectedPrimaryColumnsCount = sizeof($columns);
		$selectedColumnsCount = array();

		$query = 'SELECT ' . implode(',', $columns);
		foreach($this->childColumns AS $childColumns){
			$query .= ',' . implode(',', $childColumns);
		}

		$query .= ' FROM ' . $this->primaryORM->getDbTable();

		foreach($this->children AS $child){
			$childName = $child[self::CHILDREN_NAME];
			/** @var \Lama\ORM\Base $childORM */
			$childORM = $child[self::CHILDREN_ORM];
			$query .= ' '.$child[self::CHILDREN_JOIN_TYPE] . ' JOIN ' . $childORM->getDbTable() . ' AS ' . (self::CHILDREN_DB_TABLE_PREFIX) . $childName . ' ON (' . implode(' AND ', $child[self::CHILDREN_PARAMETERS]) . ')';
			$selectedColumnsCount[$childName]['size'] = sizeof($this->childColumns[$childName]);
			$selectedColumnsCount[$childName]['orm'] = $child[self::CHILDREN_ORM];
			$selectedColumnsCount[$childName]['ormIndex'] = $child[self::CHILDREN_ORM_INDEX];
		}
		if(!empty($this->conditions)){
			$query .= ' WHERE ' . implode(' AND ', $this->conditions);
		}

		if(!empty($this->ordering)){
			$query .= ' ORDER BY ' . implode(',', $this->ordering);
		}
		if(!empty($this->limit)){
			$query .= ' LIMIT ' . $this->limit;
			if(!empty($this->offset)){
				$query .= ', ' . $this->offset;
			}
		}

		/** @var IConnector $db */
		$db = self::getConnector();
		$results = $db->query($query, $this->params, \PDO::FETCH_NUM);

		$return = array();
		$primaryORMAliases = array_keys($this->columns);
		$this->primaryORM->getData();
        $primaryORMIndex = array_search($this->primaryORM->getPrimaryAlias(), array_values($this->primaryORM->getAllAliases()));
		foreach($results AS $row){
			$processedRow = $row;
			$primaryValue = $row[$primaryORMIndex];
            $primarySliced = array_splice($processedRow, 0, $selectedPrimaryColumnsCount);
			if(isset($return[$primaryValue])){
				$primaryORM = $return[$primaryValue];
			} else {
				/** @var \Lama\ORM\Base $primaryORM */
				$primaryORM = new $this->primaryORM;
				foreach($primaryORMAliases AS $i => $alias){
					$primaryORM->$alias = $primarySliced[$i];
				}
				$return[$primaryValue] = $primaryORM;
				$primaryORM->clearChanged();
			}

			foreach($selectedColumnsCount AS $childName => $childData){
			    $childIndex = $childData['ormIndex'];
                $primarySliced = array_splice($processedRow, 0, $childData['size']);
                if(is_null($primarySliced[$childIndex])){
                    continue;
                }
                $j = 0;
                $child = clone $childData['orm'];
                foreach(array_keys($this->childColumns[$childName]) AS $alias){
					$child->$alias = $primarySliced[$j];
					$j++;
				}
                $children = $primaryORM->$childName;
				$children[] = $child;
				$primaryORM->$childName = $children;
			}
		}

        if(!$withIndex){
            $return = array_values($return);
        }
		return $return;
	}

	public function addOrdering($column, $direction = self::ORDER_ASC)
	{
		$this->ordering[] = $this->getColumn($column) . ' ' . $direction;
	}

	public function setLimit($limit)
	{
		$this->limit = $limit;
	}

	public function setOffset($offset)
	{
		$this->offset = $offset;
	}

	public function addOr()
	{
		$args = func_get_args();
		$this->conditions[] = '(' . implode(' OR ', $args) . ')';
	}
}