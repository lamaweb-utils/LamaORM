<?php
/**
 * Created by PhpStorm.
 * User: Petrs
 * Date: 18.8.2015
 * Time: 12:08
 */

namespace Lama\Search;


use Lama\Connector\IConnector;

abstract class ASearch {
	/** @var  \Lama\ORM\Base */
	public $primaryORM;
	/** @var \Lama\ORM\Base[] */
	public $children = array();
	public $conditions = array();
	public $columns = array();
	public $childColumns = array();
	public $params = array();
	public $ordering = array();
	protected $paramInt = 0;
	protected $limit = null;
	protected $offset = null;
	/** @var \Lama\Connector\PdoMySQL */
	public static $connector = null;


	const CONDITIONS_TYPE = 'type';
	const CONDITIONS_PARAMETER = 'parameter';
	const CONDITIONS_VALUE = 'value';
	const CONDITIONS_TABLE = 'table';

	const CHILDREN_ORM = 'orm';
	const CHILDREN_NAME = 'name';
	const CHILDREN_JOIN_TYPE = 'join';
	const CHILDREN_PARAMETERS = 'parameters';
	const CHILDREN_ORM_INDEX = 'ormIndex';
	const CHILDREN_DB_TABLE_PREFIX = 'child_';

	public function __construct(\Lama\ORM\Base $primaryORM)
	{
		$this->primaryORM = $primaryORM;
		$this->columns = $primaryORM->getAllDbFields(null, true);
	}

	public static function setConnector(IConnector $connector)
	{
		self::$connector = $connector;
	}

	public static function getConnector()
	{
		return self::$connector;
	}

	protected function getColumn($parameter)
	{
		$dotPosition = strpos($parameter, '.');
		if($dotPosition !== false){
			$parameters = explode('.', $parameter, 2);
            $name = (self::CHILDREN_DB_TABLE_PREFIX) . $parameters[0] . '.' . $this->children[$parameters[0]][self::CHILDREN_ORM]->getDbField($parameters[1]);
		} else {
			$name = $this->primaryORM->getDbField($parameter, true);
		}
		return $name;
	}

	public function getAndSetParams(array $values)
	{
		$return = array();
		foreach($values AS $value){
			$return[] = $this->getAndSetParam($value);
		}
		return $return;
	}

	public function getAndSetParam($value){
		$paramName = ':par' . ($this->paramInt++) . 'am';
		$this->params[$paramName] = $value;
		return $paramName;
	}

	public function getParams()
    {
        return $this->params;
    }
	abstract public function load();
	abstract public function loadWithChildrenLoaded();
}