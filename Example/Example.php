<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 1.1.14
 * Time: 16:53
 *
 *
 * @property $id
 * @property $name
 * @property $description
 * @property $date
 * @property $hours
 * @property $perHour
 * @property $amount
 * @property $projectId
 * @property $leftToPaid
 * @property $payment
 * @property $paid
 */

class ORM_Example extends \Lama\ORM{

	public function setUp()
	{
		$this->addPrimaryColumn('a_id', 'id');
		$this->addColumn('a_name', 'name');
		$this->addColumn('a_description', 'description');
		$this->addColumn('a_date', 'date');
		$this->addColumn('a_hours', 'hours');
		$this->addColumn('a_per_hour', 'perHour');
		$this->addColumn('a_amount', 'amount');
		$this->addColumn('p_id', 'projectId');
		$this->addColumn('a_left_to_paid', 'leftToPaid');
		$this->addColumn('pl_id', 'payment');
		$this->addColumn('a_paid', 'paid');
		$this->setDbTable('copy_account');
	}
}