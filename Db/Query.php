<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 11.1.2016
 * Time: 20:12
 */

class Query extends \DB{
	protected $fetchType = \PDO::FETCH_ASSOC;

	public function fetchType($fetchType){
		$this->fetchType = $fetchType;
        return $this;
	}

	public function execute(){
		return self::$connector->query($this->query, $this->params, $this->fetchType);
	}
}