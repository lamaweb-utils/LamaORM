<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 11.1.2016
 * Time: 20:12
 */

abstract class DB {
	protected $query;
	protected $params = array();

	protected static $connector = null;

	public function __construct()
	{
		if(!self::$connector){
			$qb = \Lama\ORM\Base::getQueryBuilder();
			self::$connector = $qb->getConnector();
		}
	}

	public function query($query)
	{
		$this->query = $query;
		return $this;
	}

	public function params($params)
	{
		$this->params = $params;
		return $this;
	}
}