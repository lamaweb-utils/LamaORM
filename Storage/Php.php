<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 5.8.2015
 * Time: 22:14
 */

namespace Lama\Storage;


class Php implements \Lama\Storage\IStorage{
	protected $storage = array();
	public function isStored($className){
		return isset($this->storage[$className]);
	}

	public function store($className, \Lama\ORM\Base $orm){
		return $this->storage[$className] = array(
			'config' => $orm->getConfig(),
			'aliases' => $orm->getAllAliases(),
			'dbFields' => $orm->getAllDbFields(),
			'children' => $orm->getChildren()
		);
	}

	public function restore($className, \Lama\ORM\Base $orm){
		if(!$this->isStored($className)){
			return false;
		}
		$orm->setConfig($this->storage[$className]['config']);
		$orm->setAliases($this->storage[$className]['aliases']);
		$orm->setDbFields($this->storage[$className]['dbFields']);
		$orm->setChildren($this->storage[$className]['children']);
		return true;
	}
}