<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 5.8.2015
 * Time: 22:11
 */

namespace Lama\Storage;


interface IStorage {
	public function store($className, \Lama\ORM\Base $orm);
	public function restore($className, \Lama\ORM\Base $orm);
}