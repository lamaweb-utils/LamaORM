<?php
/**
 * Created by PhpStorm.
 * User: LamA.nOOb
 * Date: 17.5.14
 * Time: 20:16
 */

namespace Lama\Storage;

use \Lama\ORM;

class Safe {
	private static $orms = array();

	public static function getClass($class)
	{
		return self::$orms[$class];
	}

	public static function addClass(ORM $class)
	{
		self::$orms[get_class($class)] = array();
	}

	public static function addClassName($className)
	{
		self::$orms[$className] = array();
	}

	public static function classExists($class)
	{
		return (isset(self::$orms[$class]));
	}

	public static function addColumn(ORM $class, $dbField, $alias)
	{
		self::$orms[get_class($class)]['dbFields'][$alias] = $dbField;
		self::$orms[get_class($class)]['aliases'][$dbField] = $alias;
	}

	/**
	 * @param ORM $class
	 * @param string $alias
	 * @return bool
	 */
	public static function aliasExists(ORM $class, $alias)
	{
		// kouka do dbfields protoze tam jsou aliasy jako klice, spravne by bylo kontrola in_array($alias, self::$orms[get_class($class)]['aliases']) ,ale to by bylo pomalejsi
		return (isset(self::$orms[get_class($class)]['dbFields'][$alias]));
	}

	public static function dump()
	{
		dump(self::$orms);
	}

	public static function setTable(ORM $class, $table)
	{
		self::$orms[get_class($class)]['table'] = $table;
	}

	/**
	 * @param ORM $class
	 * @return mixed
	 */
	public static function getTable(ORM $class)
	{
		return self::$orms[get_class($class)]['table'];
	}

	/**
	 * vraci dbFieldy v klici a v hodnotach aliasy
	 * @param ORM $class
	 * return Array
	 */
	public static function getAllAliases(ORM $class)
	{
		return self::$orms[get_class($class)]['aliases'];
	}

	/**
	 * vraci aliasy v klici a v hodnotach dbFieldy
	 * @param ORM $class
	 * @return Array
	 */
	public static function getAllDbFields(ORM $class)
	{
		return self::$orms[get_class($class)]['dbFields'];
	}
} 